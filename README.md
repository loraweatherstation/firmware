# Firmware

Vous trouverez ci dessous des liens vers les bibliotèques que l'on vous conseille d'utiliser. 
(A noter que la bibliotèque RN2483 pour l'envoie de données en LoRa est présent dans le répertoire Send_data_Lora)


## Libraries
* https://github.com/TheThingsNetwork/arduino-device-lib/blob/master/examples/CayenneLPP/CayenneLPP.ino
* https://github.com/arduino-libraries/SD
* https://github.com/EnviroDIY/Arduino-SDI-12
